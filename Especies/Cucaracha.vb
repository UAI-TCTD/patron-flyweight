﻿Public Class Cucaracha
    Inherits Especies

    Dim _patas As Integer
    Dim _color As String

    Public Overrides Function Color() As String
        Return "marron"
    End Function

    Public Overrides Function Patas() As String
        Return 6
    End Function

    Public Overrides Function ToString() As String
        Return "Cucaracha"
    End Function
End Class
