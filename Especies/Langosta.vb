﻿Public Class Langosta
    Inherits Especies

    Public Overrides Function Color() As String
        Return "verde"
    End Function

    Public Overrides Function Patas() As String
        Return "6"
    End Function
    Public Overrides Function ToString() As String
        Return "Langosta"
    End Function
End Class
