﻿Public Class Hormiga
    Inherits Especies

    Public Overrides Function Color() As String
        Return "rojo"
    End Function

    Public Overrides Function Patas() As String
        Return 6
    End Function
    Public Overrides Function ToString() As String
        Return "Hormiga"
    End Function
End Class
