﻿Public Class Fabrica
    Private _listaEspecies As New Dictionary(Of Integer, Especies)

    Public Sub New()
        Dim _cuca As New Cucaracha
        Dim _homero As New Hormiga
        Dim _lango As New Langosta

        _listaEspecies.Add(1, _cuca)
        _listaEspecies.Add(2, _homero)
        _listaEspecies.Add(3, _lango)

    End Sub


    Public Function Agregar(valor As Integer) As Especies
        Return _listaEspecies(valor)
    End Function

End Class
