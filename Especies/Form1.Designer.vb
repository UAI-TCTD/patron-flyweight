﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.rbtnCucaracha = New System.Windows.Forms.RadioButton()
        Me.rbtnHormiga = New System.Windows.Forms.RadioButton()
        Me.rbtnLangosta = New System.Windows.Forms.RadioButton()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(40, 81)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(78, 36)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'rbtnCucaracha
        '
        Me.rbtnCucaracha.AutoSize = True
        Me.rbtnCucaracha.Location = New System.Drawing.Point(41, 31)
        Me.rbtnCucaracha.Name = "rbtnCucaracha"
        Me.rbtnCucaracha.Size = New System.Drawing.Size(77, 17)
        Me.rbtnCucaracha.TabIndex = 1
        Me.rbtnCucaracha.TabStop = True
        Me.rbtnCucaracha.Text = "Cucaracha"
        Me.rbtnCucaracha.UseVisualStyleBackColor = True
        '
        'rbtnHormiga
        '
        Me.rbtnHormiga.AutoSize = True
        Me.rbtnHormiga.Location = New System.Drawing.Point(176, 31)
        Me.rbtnHormiga.Name = "rbtnHormiga"
        Me.rbtnHormiga.Size = New System.Drawing.Size(64, 17)
        Me.rbtnHormiga.TabIndex = 2
        Me.rbtnHormiga.TabStop = True
        Me.rbtnHormiga.Text = "Hormiga"
        Me.rbtnHormiga.UseVisualStyleBackColor = True
        '
        'rbtnLangosta
        '
        Me.rbtnLangosta.AutoSize = True
        Me.rbtnLangosta.Location = New System.Drawing.Point(315, 31)
        Me.rbtnLangosta.Name = "rbtnLangosta"
        Me.rbtnLangosta.Size = New System.Drawing.Size(69, 17)
        Me.rbtnLangosta.TabIndex = 3
        Me.rbtnLangosta.TabStop = True
        Me.rbtnLangosta.Text = "Langosta"
        Me.rbtnLangosta.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(30, 142)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(626, 247)
        Me.TextBox1.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 401)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.rbtnLangosta)
        Me.Controls.Add(Me.rbtnHormiga)
        Me.Controls.Add(Me.rbtnCucaracha)
        Me.Controls.Add(Me.btnAgregar)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents rbtnCucaracha As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnHormiga As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnLangosta As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox

End Class
